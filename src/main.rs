use actix_cors::Cors;
use actix_web::{web, App, http, HttpRequest, HttpResponse, HttpServer, Responder};
use reqwest::Client;
use scraper::{Html, Selector};
use serde::Deserialize;
use regex::Regex;
use url::Url;

#[derive(Deserialize)]
struct FetchQuery {
    url: Option<String>,
}

fn get_base_url(full_url: &str) -> Option<String> {
    match Url::parse(full_url) {
        Ok(parsed_url) => {
            let mut base_url = format!("{}://{}", parsed_url.scheme(), parsed_url.host_str()?);
            if base_url.ends_with("/") {
                base_url.pop();
            }
            Some(base_url)
        },
        Err(_) => None,
    }
}

fn update_external_links(html_content: &str, base_url: &str) -> String {
    let href_regex = Regex::new(r#"href="([^"]+)"#).unwrap();
    
    href_regex.replace_all(html_content, |caps: &regex::Captures| {
        let base = &caps[1];
        if base.starts_with("http://") || base.starts_with("https://") {
            format!("href=\"{}\"", base)
        } else {
            format!("href=\"{}/{}\"", base_url, base)
        }
    }).to_string()
}

fn update_image_sources(html_content: &str, base_url: &str) -> String {
    let img_regex = Regex::new("(<img [^>]*src=\")([^\"]+)").unwrap();

    img_regex.replace_all(html_content, |caps: &regex::Captures| {
        format!("{}{}/{}\" style=\"max-width:100%\"", &caps[1], base_url, &caps[2])
    }).to_string()
}

async fn hello_world() -> impl Responder {
    HttpResponse::Ok().body("Helloo")
}

async fn fetch_url(query: web::Query<FetchQuery>, req: HttpRequest) -> impl Responder {
    if req.method() == http::Method::OPTIONS {
        eprintln!("{}", req.method().to_string());
        return HttpResponse::Ok().finish();
    }

    //let client = Client::new();
    let client = match Client::builder()
        .redirect(reqwest::redirect::Policy::none())
        .build() {
            Ok(client) => client,
            Err(_) => {
                return HttpResponse::InternalServerError().finish();
            }
    };

    match &query.url {
        Some(url) if !url.is_empty() => {
            let mut url = url.clone();
            //if url.ends_with('/') {
                //url.pop();
            //}
            match client.get(&url).send().await {
                Ok(resp) => match resp.text().await {
                    Ok(text) => {
                        eprintln!("{} {}", text, url);
                        let html = Html::parse_document(&text);
                        let body_selector = Selector::parse("body").unwrap();
                        let body = html.select(&body_selector).next();
                        let body_html = body.map_or(String::new(), |b| b.inner_html());
                        let base_url = get_base_url(&url).unwrap_or_else(|| String::from(""));
                        let mut modified_html = update_image_sources(&body_html, &base_url); 
                        modified_html = update_external_links(&modified_html, &base_url);               
                        HttpResponse::Ok().content_type("text/html").body(modified_html)
                    }
                    Err(_) => HttpResponse::InternalServerError().finish(),
                },
                Err(_) => HttpResponse::InternalServerError().finish(),
            }
        }
        _ => {
            let html_string = "<body>Welcome to the Home Page</body>";
            HttpResponse::Ok().content_type("text/html").body(html_string)
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        let cors = Cors::default()
            .allow_any_method()
            .allowed_origin("http://localhost:3000")
            .allowed_origin("https://a274-184-170-241-39.ngrok-free.app")
            .allow_any_header()
            .max_age(3600);

        App::new()
            .wrap(cors)
            .route("/fetch", web::get().to(fetch_url))
            .route("/hello", web::get().to(hello_world))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
